import mongoose from '../config/database';

const schema = mongoose.Schema({
  picture: String,
  name: String,
  longitude: String,
  latitude: String,
  fbId: String,
  email: String,
  liked: [String]
})

export default mongoose.model('streetUser', schema);