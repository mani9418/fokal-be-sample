import mongoose from '../config/database';

const schema = mongoose.Schema({
  picture: [String],
  title: String,
  category: String,
  longitude: String,
  latitude: String,
  price: String,
  description: String,
  views: Number,
  location: { type: [Number, Number], index: '2dsphere' },
  active: Boolean,
})

export default mongoose.model('item', schema);