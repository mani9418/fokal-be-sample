import * as repoItem from '../repositories/item';
import * as repoStreetUser from '../repositories/streetUser';

export const createItem = async (payload) => {


  let itemCreated = await repoItem.CreateItem(payload);
  itemCreated = itemCreated.toJSON();
  return itemCreated;

}

export const getAll = async (type = {}) => {
  let allItems = await repoItem.GetAll(type);
  allItems = allItems.map(item => ({
    _id: item._id,
    category: item.category,
    description: item.description,
    latitude: item.latitude,
    longitude: item.longitude,
    price: item.price,
    title: item.title,
    views: item.views,
    location: item.location,
    active: item.active,
    picture: item.picture ? item.picture[0] : null
  }));
  return allItems;
}

export const getById = (payload) => {
  return repoItem.GetAndUpdate({ _id: payload.id });
}

export const addUser = async (payload) => {
  let userCreated = await repoStreetUser.CreateUser(payload);
  return userCreated;
}

export const addFavorite = async (payload) => {
  let userCreated = await repoStreetUser.AddFav(payload);
  return userCreated;
}

export const getByUser = async (payload) => {
  let userCreated = await repoStreetUser.GetByUser(payload);
  return userCreated;
}

export const deleteItem = async (payload) => {
  let userCreated = await repoItem.DeletedItem(payload);
  return userCreated;
}

export const update = async (payload) => {
  let userCreated = await repoItem.update(payload);
  return userCreated;
}

export const getImages = async (payload) => {
  let userCreated = await repoItem.getImages(payload);
  return userCreated;
}

export const deleteImage = async (payload) => {
  let userCreated = await repoItem.deleteImage(payload);
  return userCreated;
}