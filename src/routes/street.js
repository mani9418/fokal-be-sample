import Router from 'koa-router';
import * as streetCtrl from '../controller/street';

const routerStreet = new Router();

routerStreet.get('/street/image/:id', streetCtrl.getOne);
routerStreet.post('/upload', streetCtrl.upload);
routerStreet.post('/street/user', streetCtrl.addUser);
routerStreet.post('/street/addFav', streetCtrl.addFavorite);
routerStreet.get('/street/likedByMe/:id', streetCtrl.likedByMe);
routerStreet.post('/street', streetCtrl.post);
routerStreet.get('/street', streetCtrl.getAll);
routerStreet.put('/street/:id', streetCtrl.update);
routerStreet.delete('/street/:id', streetCtrl.deleteItem);
routerStreet.get('/street/images/:id', streetCtrl.getImages);
routerStreet.delete('/street/image/:id', streetCtrl.deleteImage);

export default routerStreet.routes();