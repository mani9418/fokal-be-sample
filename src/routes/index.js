import compose from 'koa-compose';
import routerStreet from './street';


const protectedRoutes = () => compose([
]);

const unprotectedRoutes = () => compose([
    routerStreet
]);

export {
    unprotectedRoutes,
    protectedRoutes
}