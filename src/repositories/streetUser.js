import streetUserModel from '../model/streetUser';
import itemModel from '../model/item';

export const CreateUser = async (payload) => {
  let newUser;
  const user = await streetUserModel.findOne({ fbId: payload.fbId });
  if (!user) {
    newUser = new streetUserModel(payload);
    return newUser.save();
  }
  return user;
}
export const AddFav = async (payload) => {
  console.log('AddFav', payload.userId);
  const updateData = await streetUserModel.update(
    { fbId: payload.userId },
    { $addToSet: { liked: payload.itemId } }
  );
  if (updateData.nModified !== 1) await streetUserModel.update(
    { fbId: payload.userId },
    { $pull: { liked: payload.itemId } }
  );
  return updateData;
}
export const GetByUser = async (userId) => {
  console.log('fbiD', userId);
  const user = await streetUserModel.findOne({ fbId: userId});
  console.log('all user', user);
  return itemModel.find({ _id: { $in: user.liked } })
}
