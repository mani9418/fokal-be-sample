import itemModel from '../model/item';

export const CreateItem = (payload) => {
  const item = new itemModel(payload);
  return item.save();
}

export const GetAll = async (param = {}) => {
  // return itemModel.find({}).limit(50)
  // const allItems = await itemModel.find(param);
  // const promises = allItems.map(item => {
  //   return itemModel.update(
  //     { _id: item._id },
  //     {
  //       $set: {
  //         location: [+item.latitude, +item.longitude]
  //       }
  //     }
  //   )
  // });
  // return Promise.all(promises);
  let conditions = {
    active: true
  };
  if (param.type) {
    conditions.category = param.type;
  }
  if (param.lat && param.lng) {
    conditions.location = {
      $near: {
        $geometry: {
          type: 'Point',
          coordinates: [param.lat, param.lng]
        },
        $maxDistance: 3000
        //in meters
      }
    };
  }
console.log('parms::111::', param);
  return itemModel.find({
    active: true
  });

}

export const GetAndUpdate = async (param = {}) => {
  await itemModel.update(
    { _id: param },
    { $inc: { views: 1 } }
  );
  return itemModel.findOne(param);
}

export const DeletedItem = async (param = {}) => {
  return  itemModel.remove(
    { _id: param },
  );
}

export const getImages = async (id) => {
  return itemModel.find({
    _id: id
  }, 'picture');
}

export const deleteImage = async (payload) => {
  return itemModel.update(
    {_id: payload.itemId} ,
    {$pull: { picture: payload.pictureUrl }}
  );
}
