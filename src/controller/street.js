import { awsConfig } from './../../secret-bucket';
import * as serviceStreet from '../services/streetItems';
import shortid from 'shortid';
const AWS = require('aws-sdk');

export const post = async (ctx, next) => {
console.log('all payload', (ctx.request.body));
  const payload = {
    picture: (ctx.request.body.picturePath),
    title: (ctx.request.body.title),
    category: (ctx.request.body.category),
    location: [(ctx.request.body.latLng.latitude),(ctx.request.body.latLng.longitude)],
    description: (ctx.request.body.description),
    price: (ctx.request.body.price),
    longitude: (ctx.request.body.latLng.longitude),
    latitude: (ctx.request.body.latLng.latitude),
    active: ctx.request.body.active,
  }
  const promise = await serviceStreet.createItem(payload);
  ctx.body = promise;
};


export const getAll = async (ctx) => {
  const promise = await serviceStreet.getAll(ctx.request.query);
  // let bitmap = null;
  // promise.forEach(item => {
  //   bitmap = fs.readFileSync(`./public/${item.picture}.png`)
  //   item.picture = new Buffer(bitmap).toString('base64');
  // })
  ctx.body = promise;
};

export const upload = async (ctx) => {

  AWS.config.update({ accessKeyId: awsConfig.access_id, secretAccessKey: awsConfig.access_key });
  const s3 = new AWS.S3();
  var imgData = ctx.request.body.image ;
  const base64Data = new Buffer(imgData.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const uniqueId = shortid.generate();
  const imagePath = `street/${uniqueId}.jpg`;
  const params = {
    Bucket: 'ideamatch.me',
    Key: imagePath, // type is not required
    Body: base64Data,
    ACL: 'public-read',
    ContentEncoding: 'base64', // required
    ContentType: `image/jpg` // required. Notice the back ticks
  }
  // require('fs').writeFile(`./public/${uniqueId}.png`, base64Data, 'base64',
  //   function (err, data) {
  //     if (err) {
  //       console.log('err', err);
  //     }
  //     console.log('success');
  //   });
  // ctx.body = {
  //   imagePath: uniqueId
  // }
  console.log('here');

  s3.upload(params, (err, data) => {
    if (err) {  console.log(err) }

    // Continue if no error
    // Save data.Location in your database
    console.log('on S3 upload:: ', data);
  });

  ctx.body = {
    imagePath: awsConfig.s3Path + imagePath
  }
  // Do validation, but files are already uploading...

};

export const getOne = async (ctx) => {
  let promise = await serviceStreet.getById(ctx.params);
  console.log('promise::', promise);
  // promise.forEach(item => {
  //   bitmap = fs.readFileSync(`./public/${item.picture}.png`)
  //   item.picture = new Buffer(bitmap).toString('base64');
  // })
  ctx.body = promise;
}

export const addUser = async (ctx) => {
  const payload = {
    email: (ctx.request.body.email),
    name: (ctx.request.body.name),
    longitude: (ctx.request.body.longitude),
    latitude: (ctx.request.body.latitude),
    fbId: (ctx.request.body.id),
    picture: (ctx.request.body.picture),
  }
  let promise = await serviceStreet.addUser(payload);
  ctx.body = promise;
}

export const addFavorite = async (ctx) => {
  console.log('bdy::', ctx.request.body);
  let promise = await serviceStreet.addFavorite(ctx.request.body);
  ctx.body = promise;
}



export const likedByMe = async (ctx) => {
  let promise = await serviceStreet.getByUser(ctx.params.id);
  ctx.body = promise;
}


export const deleteItem = async (ctx) => {
  let promise = await serviceStreet.deleteItem(ctx.params.id);
  ctx.body = promise;
}

export const update = async (ctx) => {
  let promise = await serviceStreet.update({
    itemId: ctx.params.id,
    item: ctx.request.body
  });
  ctx.body = promise;
}

export const getImages = async (ctx) => {
  let promise = await serviceStreet.getImages(ctx.params.id);
  ctx.body = promise;
}

export const deleteImage = async (ctx) => {
  let promise = await serviceStreet.deleteImage({
    itemId: ctx.params.id,
    pictureUrl: ctx.request.body.pictureUrl
  });
  ctx.body = promise;
}
